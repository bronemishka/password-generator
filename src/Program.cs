using System;
using System.IO;

namespace PasswordGenerator
{
    public class GeneratePassword
    {
        private static string _generatedPass;

        public string MakePass(int passLen, Random rnd)
        {
            _generatedPass = string.Empty;
            for (var i = 0; i < passLen; i++)
            {
                switch (rnd.Next(1, 4))
                {
                    case (1):
                        _generatedPass += ((char)rnd.Next(65, 91)).ToString(); //Generate from A-Z
                        break;
                    case (2):
                        _generatedPass += ((char)rnd.Next(97, 123)).ToString(); //Generate from a-z
                        break;
                    case (3):
                        _generatedPass += ((char)rnd.Next(48, 58)).ToString(); //Generate from 0-9
                        break;
                    default:
                        _generatedPass += string.Empty;
                        break;
                }
            }
            return _generatedPass;
        }
    }

    internal class Program
    {
        private static void HelloThere(string passGenVersion)
        {
            passGenVersion += "\n";
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(" __   __   __        ___          __                 \n|__) |__) /  \\ |\\ | |__   |\\/| | /__` |__| |__/  /\\  \n|__) |  \\ \\__/ | \\| |___  |  | | .__/ |  | |  \\ /~~\\ \n\n __        __   __        __   __   __\n|__)  /\\  /__` /__` |  | /  \\ |__) |  \\\n|    /~~\\ .__/ .__/ |/\\| \\__/ |  \\ |__/\n\n __   ___       ___  __       ___  __   __\n/ _` |__  |\\ | |__  |__)  /\\   |  /  \\ |__)\n\\__> |___ | \\| |___ |  \\ /~~\\  |  \\__/ |  \\\n");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("ver: " + passGenVersion);
        }
        private static void ColorWrite(string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ForegroundColor = ConsoleColor.White;
        }
        private static void ErrorHandler(string error)
        {
            ColorWrite(error, ConsoleColor.Red);
        }
        private static void WritePasswordsToFile(string password)
        {
            try
            {
                ColorWrite("\n\nName of file(*.txt will be auto-added): ", ConsoleColor.White);
                Console.ForegroundColor = ConsoleColor.Magenta;
                var fileName = Console.ReadLine();
                if (File.Exists(fileName + ".txt")) File.Delete(fileName + ".txt");
                File.WriteAllText(fileName + ".txt", password);
                ColorWrite(fileName + ".txt created!", ConsoleColor.Green);
            }
            catch
            {
                ErrorHandler("Error while creating file.");
            }
        }
        private static string GenPass(int len, int amt)
        {
            ColorWrite("\n\n===========BEGIN OF PASSWORD GENERATION===========", ConsoleColor.DarkGray);
            var rnd = new Random();
            Console.ForegroundColor = ConsoleColor.Gray;
            var passwords = string.Empty;
            var password = new GeneratePassword();
            for (var j = 0; j < amt; j++)
            {
                passwords += "\n" + (j + 1) + ") " + password.MakePass(len, rnd);
            }
            ColorWrite(passwords, ConsoleColor.DarkGray);
            ColorWrite("\n============END OF PASSWORD GENERATION============", ConsoleColor.DarkGray);
            return passwords;
        }
        private static void Main()
        {
            HelloThere("1.0");
            ColorWrite("Length of password: ", ConsoleColor.White);
            Console.ForegroundColor = ConsoleColor.Magenta;
            var passSizeStr = Console.ReadLine();
            if (int.TryParse(passSizeStr, out var passSize)) ColorWrite($"Length set to {passSize}", ConsoleColor.Green);
            else 
            {
                ErrorHandler("Wrong input. Size would be set to 16.");
                passSize = 16;
            }
            ColorWrite("\n\nHow many passwords to generate: ", ConsoleColor.White);
            Console.ForegroundColor = ConsoleColor.Magenta;
            var passAmountStr = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Green;
            if (int.TryParse(passAmountStr, out var passAmount)) ColorWrite($"Amount set to {passAmount}", ConsoleColor.Green);
            else
            {
                ErrorHandler("Wrong input. One password will be generated.");
                passAmount = 1;
            }
            var passString = GenPass(passSize, passAmount);
            try
            {
                var pressedKey = ConsoleKey.A;
                while (pressedKey != ConsoleKey.Y && pressedKey != ConsoleKey.N)
                {
                    ColorWrite("\n\nSave generated passwords to file?[Y/N]", ConsoleColor.White);
                    pressedKey = Console.ReadKey(true).Key;
                    switch (pressedKey)
                    {
                        case ConsoleKey.Y:
                            WritePasswordsToFile(passString);
                            break;
                        case ConsoleKey.N:
                            break;
                    }
                }
            }
            catch
            {
                ErrorHandler("Problems appeared while generating.");
            }
            ColorWrite("\n\nClosing after key press.", ConsoleColor.DarkGray);
            Console.ReadKey();
        }
    }
}